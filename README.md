# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###
This is the new magento 2 repository for snowcountry



## prerequisites ##
- Update composer to the latest version
- PHP 7.2 (installer failed on 7.3) -> see below (once we upgrade to magento 2.4 this should work)



## How do I get set up? ###

- clone git repo `git clone https://thijsdejong@bitbucket.org/thijsdejong/snowcountry-magento-2.git` (Evt. kan je je public key toevoegen)
- Install dependencies `composer install`

-- update permissions inside your box:
    `cd httpdocs`
    `find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +`
    `find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +`
    `chmod u+x bin/magento`

- Create database using your favorite tool and use the credentials in the install command
- Install magento `bin/magento setup:install \
					--base-url=https://m2dev.snowcountry.nl \
					--db-host=localhost \
					--db-name=vagrant \
					--db-user=vagrant \
					--db-password=vagrant \
					--admin-firstname=admin \
					--admin-lastname=admin \
					--admin-email=admin@admin.com \
					--admin-user=admin \
					--admin-password=admin \
					--language=en_US \
					--currency=EUR \
					--timezone=Europe/Amsterdam \
					--use-rewrites=1`

# Deploy static content 
` bin/Magento setup:static-content:deploy nl_NL -f`
` bin/Magento setup:static-content:deploy -f`

// More specifics on development environment will be specified later (probably Docker)

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
`git pull`
`bin/magento composer install`
`bin/magento setup:di:compile`
`bin/magento setup:migrate`
`bin/magento c:f`


....

# how do i upgrade PHP to 7.x on vagrant to be compatible with m2
`apt-get update`

//you can also choose another version., if you repeat this one line for 7.3 + 7.4 you have the versions already installed and can switch easly later

`sudo apt-get install php7.2 php7.2-fpm php7.2-dev php7.2-bcmath php7.2-common php7.2-curl php7.2-gd php7.2-mbstring php7.2-pdo php7.2-mysql php7.2-xml php7.2-xmlrpc php7.2-xsl php7.2-zip php7.2-soap php7.2-phpdbg php7.2-opcache php7.2-json php7.2-intl php7.2-json php7.2-iconv`


//config voor PHP cli

`sudo update-alternatives --config php`  -> select 7.2 for now

//Switch FPM service socket. Now 7.2 listens on 127.0.0.1 (via php-fpm.conf) and 7.0 is disabled

`sudo cp /etc/php/7.0/fpm/php-fpm.conf /etc/php/7.2/fpm/php-fpm.conf`

`sudo cp /etc/php/7.0/fpm/common.* /etc/php/7.2/fpm/`

`sudo /etc/init.d/php7.0-fpm stop`

`sudo /etc/init.d/php7.2-fpm start`



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact